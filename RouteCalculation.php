<?php


class RouteCalculation
{
    public function calculate($origin, $destination)
    {
        $key = '****';
        $url = 'https://maps.googleapis.com/maps/api/directions/json?origin=%s&destination=%s&key=%s';
        $data = json_decode(file_get_contents(sprintf($url, $origin, $destination, $key)), true);
        list($originCity, $originCountry) = explode(',', $origin);
        $routeInformation = $data['routes'][0]['legs'][0];
        $this->displayBasicInfo($routeInformation);
        $distancePerCountry = $this->calculateDistancePerCountry($routeInformation['steps'], $originCountry);
        $this->displayDistancePerCountry($distancePerCountry);
    }

    public function displayBasicInfo($routeInformation)
    {
        echo '<table border="1">';
        echo sprintf('<tr><td>Total Distance</td><td>%s</td></tr>', $routeInformation['distance']['text']);
        echo sprintf('<tr><td>Total Duration</td><td>%s</td></tr>', $routeInformation['duration']['text']);
        echo sprintf('<tr><td>Start Address</td><td>%s</td></tr>', $routeInformation['start_address']);
        echo sprintf('<tr><td>End Address</td><td>%s</td></tr>', $routeInformation['end_address']);
        echo '</table>';
    }

    public function displayDistancePerCountry($distancePerCountry)
    {
        echo '<table border="1">';

        foreach ($distancePerCountry as $country => $distanceInfo) {
            echo sprintf('<tr><td>%s</td><td>%s</td><td>%s</td></tr>',
                $country,
                $this->formatDistance($distanceInfo),
                $this->formatRoute($distanceInfo),
            );
        }

        echo '</table>';
    }

    public function formatDistance($distanceInfo)
    {
        return ($distanceInfo['distance'] / 1000) . ' Km';
    }

    public function formatRoute($distanceInfo)
    {
        return sprintf('<a href="https://www.google.com/maps/dir/%s/%s" target="_blank">Route</a>',
            $distanceInfo['startLocation'],
            $distanceInfo['endLocation']);
    }

    public function calculateDistancePerCountry($steps, $originCountry)
    {
        $currentCountry = $originCountry;
        $distance[$currentCountry]['distance'] = 0;

        foreach ($steps as $key => $step) {

            $distance[$currentCountry]['distance'] += $step['distance']['value'];

            if ($key === 0) {
                $distance[$currentCountry]['startLocation'] =
                    sprintf('%s,%s', $step['start_location']['lat'], $step['start_location']['lng']);
            }

            $distance[$currentCountry]['endLocation'] =
                sprintf('%s,%s', $step['end_location']['lat'], $step['end_location']['lng']);

            if (strpos($step['html_instructions'], 'Entering') !== false) {
                $currentCountry = $this->extractCountryName($step['html_instructions']);
                $distance[$currentCountry]['distance'] = 0;
                $distance[$currentCountry]['startLocation'] =
                    sprintf('%s,%s', $step['end_location']['lat'], $step['end_location']['lng']);
            }
        }

        return $distance;
    }

    public function extractCountryName($string)
    {
        preg_match('/Entering\s?([a-zA-z\s]+)/', $string, $match);

        return $match[1];
    }
}

$origin = $argv[1];
$destination = $argv[2];
$routeCalculation = new RouteCalculation();
$routeCalculation->calculate($origin, $destination);
